# Presentation
SwiftCmd \[swɪft.kəmænd\] is a simple program to bookmark shell commands.

# Installing
Just copy *sc.py* to whatever path (system or not) you want to execute it from.

The following command (ran with root privileges) should work under Linux:
```sh
cp sc.py /usr/bin/sc && chmod a+x /usr/bin/sc
```

# Usage
## Configuration
You might want to alias `sc` to something even easier to type, using your favorite shell alias functionality.

SwiftCmd stores its data in file called *~/.sc.json*.

## Limitations
SwiftCmd executes commands directly, ignoring your shell, which has the following implications:
- Shell commands that are not actual programs but internal commands, like `cd`, will not work. You may still bookmark them but you will need to use SwiftCmd's `print` function and copy the commands to your CLI manually.
- Aliases your usual shell has, including the default aliases like the `--color=auto` ones, will not work.
- Shell special tokens, like `&&` or `|`, will not work. SwiftCmd provides you with similar built-in functions.

## Commands
```sh
sc _n [RANK]? [NAME] [CMD+ARGS]
```
```sh
sc _new [RANK]? [NAME] [CMD+ARGS]
```
Add a new bookmark \[NAME\] for command \[CMD+ARGS\]. If \[RANK\] is given, the command will be put at this rank in the list of bookmarked commands (else at the end).

\[CMD+ARGS\] is to be written as you usually would in a shell with one exception. Numeric values for \[NAME\] are forbidden, names starting with `_` should also be avoided (reserved for built-in commands).

Arguments of the form `_[INTEGER]` indicate that they must be replaced by the argument at position \[INTEGER\] when calling the bookmark. You can also use `_-` to take all arguments after the last numbered argument. Any (non-formatted) argument prefixed with `_p`, `_n` or `_=` will be concatenated to the previous, next or both argument(s). You can use `__` as an escape for `_` if needed.

You may add the additional prefix `/` for arguments to concatenate (e.g. `_/p`). If this prefix is added, the argument will only be passed if the argument it is to be concatenated to is present.

Commands can be broken into multiple parts with `_c`, `_s`, `_a` and `_v`. These special arguments will respectively cause the commands to be executed: concurrently, in sequence, in sequence only if the previous succeed, in sequence only if the previous fails.

You may use the parameter of the form `_o[FILE]` to use \[FILE\] as output and `_i[FILE]` as input. You can replace `o` by `x` if you want the command to fail if \[FILE\] exists, or `+` to append to \[FILE\] rather than overwriting it.

`_l` will break the command and pass the output of the left par to the right part.

All output-related arguments can be prefixed by `e` (e.g. `_eo[FILE]`) to include stderr in addition to stdout.

```sh
sc _i [RANK] [CMD+ARGS]
```
```sh
sc _insert [RANK] [CMD+ARGS]
```
Add a new nameless bookmark at rank \[RANK\] for command \[CMD+ARGS\]. For more details about the syntax of \[CMD+ARGS\], see the command `_n`.
\[CMD+ARGS\] is to be written as you usually would in a shell with one exception.

```sh
sc _p [CMD+ARGS]
```
```sh
sc _push [CMD+ARGS]
```
Add a new nameless bookmark for command \[CMD+ARGS\].
\[CMD+ARGS\] is to be written as you usually would in a shell with one exception. For more details about the syntax of \[CMD+ARGS\], see the command `_n`.

```sh
sc _a [NAME] [MARK]
```
```sh
sc _add [NAME] [MARK]
```
Alias bookmark \[MARK\] to \[NAME\]. \[MARK\] may be either a name or the rank of the bookmark. Numeric values for \[NAME\] are forbidden.

```sh
sc _d [NAME]
```
```sh
sc _del [NAME]
```
Remove bookmark name \[NAME\]. The bookmark itself is kept.

```sh
sc _r [MARK]
```
```sh
sc _rm [MARK]
```
Remove bookmark \[MARK\]. \[MARK\] may be either a name or the rank of the bookmark.

```sh
sc _rk [MARK] [RANK]
```
```sh
sc _rank [MARK] [RANK]
```
Move bookmark \[MARK\] to rank \[RANK\]. \[MARK\] may be either a name or the rank of the bookmark.

```sh
sc _nm [OLD] [NEW]
```
```sh
sc _name [OLD] [NEW]
```
Rename bookmark \[OLD\] to \[NEW\].

```sh
sc _l
```
```sh
sc _list
```
Print a list of all bookmarks.

```sh
sc _pr [MARK]
```
```sh
sc _print [MARK]
```
Print bookmark \[MARK\]. \[MARK\] may be either a name or the rank of the bookmark.

```sh
sc _f [TERM]
```
```sh
sc _find [TERM]
```
Print bookmarks of commands containing \[TERM\].

```sh
sc _s [MARK] [ARG] [NAME] [VAL]
```
```sh
sc _short [MARK] [ARG] [NAME] [VAL]
```
Add shorthand \[NAME\] for value \[VAL\] of argument \[ARG\] to bookmark \[MARK\]. \[MARK\] may be either a name or the rank of the bookmark. \[ARG\] is the number of the argument (must be numbered or `-`).

```sh
sc _sd [MARK] [ARG] [NAME]
```
```sh
sc _short-del [MARK] [ARG] [NAME]
```
Delete shorthand \[NAME\] for value \[VAL\] of argument \[ARG\] to bookmark \[MARK\]. \[MARK\] may be either a name or the rank of the bookmark. \[ARG\] is the number of the argument (must be numbered or `-`).

```sh
sc _sk [MARK] [ARG]?
```
```sh
sc _short-clear [MARK] [ARG]?
```
Delete all shorthands from bookmark \[MARK\]. Limited to argument \[ARG\] is given. \[MARK\] may be either a name or the rank of the bookmark. \[ARG\] is the number of the argument (must be numbered or `-`).

```sh
sc _e [MARK] [ARG] [VAL]?
```
```sh
sc _def [MARK] [ARG] [VAL]?
```
Add default value \[VAL\] of argument \[ARG\] to bookmark \[MARK\]. \[MARK\] may be either a name or the rank of the bookmark. \[ARG\] is the number of the argument (must be numbered or `-`). If \[VAL\] is not given, the default value is removed.

```sh
sc _dc [MARK] [ARG]?
```
```sh
sc _def-clear [MARK] [ARG]?
```
Delete all default values from bookmark \[MARK\]. Limited to argument \[ARG\] is given. \[MARK\] may be either a name or the rank of the bookmark. \[ARG\] is the number of the argument (must be numbered or `-`).

```sh
sc _c [MARK] [CMD+ARGS]
```
```sh
sc _change [MARK] [CMD+ARGS]
```
Change the command bookmarked under \[MARK\]. \[MARK\] may be either a name or the rank of the bookmark. \[CMD+ARGS\] work the same way as `_new`.

```sh
sc _t [MARK] [ARGS]
```
```sh
sc _test [MARK] [ARGS]
```
Print the command bookmarked under \[MARK\]. \[MARK\] may be either a name or the rank of the bookmark. \[ARGS\] will be passed to the command, by default at the end or at positions indicated when defining.

```sh
sc _sr [FILE]?
```
```sh
sc _source [FILE]?
```
Set \[FILE\] as SwiftCmd's data file, instead of ~/.sc.json. Note that ~/.sc.json must be kept as it still stores \[FILE\]'s path.

Once this is used your bookmarks will be those in \[FILE\] and only these. You will need write access to \[FILE\] to make any change. Bookmarks present in ~/.sc.json are kept and used as (read only) fallback if \[FILE\] is not accessible.

If \[FILE\] is not given, reset to default.

You may not use this if \[FILE\] is also linked to another data file (no chained sourcing).

```sh
sc [MARK] [ARGS]
```
Execute the command bookmarked under \[MARK\]. \[MARK\] may be either a name or the rank of the bookmark. \[ARGS\] will be passed to the command, by default at the end or at positions indicated when defining.

# License
This project is placed under protection of the GNU General Public License version 3 or later as published by the Free Software Foundation.

## Contributions
Contributions are welcome. By contributing to this project, you accept that your contribution is GPLv3+.

# Support
This project is in (early) **beta** state.

This project is currently maintained by a single developer on free time with limited testing possibilities; do not expect professional-grade support. You can fill issues but there is no warranty that they will be addressed. Please try to make reports as good as possible (information to reproduce is mandatory, providing a tested fix is great).

All recent POSIX compliant systems should be supported. Non-POSIX-compliant systems, including MS Windows, are **not** officially supported. Non-FOSS POSIX-compliant systems, including macOS, will **not** receive any special attention a GNU/Linux or *BSD system could expect (no fix for macOS-only issues).

## Dependencies
- Python ⩾3.7

# Code of Conduct
Please follow the [KDE Community Code of Conduct](https://kde.org/code-of-conduct/)
