#!/usr/bin/env python3
import sys
import os
import subprocess
import json


ECHAR = '_'
TAB_VAL = 4
CONF_FILE = os.path.expanduser("~/.sc.json")


def numArg(arg):
    if arg.isnumeric():
        return int(arg)-1
    else:
        return arg

def isNumArg(arg):
    return isinstance(arg,int)

def isEndArg(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (arg[0] == '-') and (len(arg) == 1)

def isNumDict(arg):
    return isinstance(arg,list) and isinstance(arg[0],int)

def isEndDict(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (arg[0] == '-') and (len(arg) > 1)

def hasDefault(arg):
    return isinstance(arg,list) and (len(arg) > 2)

def isArg(arg):
    return isNumArg(arg) or isEndArg(arg)

def isDict(arg):
    return isNumDict(arg) or isEndDict(arg)

def isComp(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (('p' in arg[0]) or ('n' in arg[0]) or ('=' in arg[0]))

def isCompPre(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (('p' in arg[0]) or ('=' in arg[0]))

def isCompNxt(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (('n' in arg[0]) or ('=' in arg[0]))

def isCompOpt(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and ('/' in arg[0])

def eqArg(arg,val):
    if isinstance(arg,list):
        return (str(arg[0]) == str(val))
    else:
        return (str(arg) == str(val))

def isErrRed(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and ('e' in arg[0])

def isFileRep(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and ('o' in arg[0])

def isFilePro(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and ('x' in arg[0])

def isFileApp(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and ('+' in arg[0])

def isFileIn(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (arg[0] == 'i')

def isFileOut(arg):
    return isFileRep(arg) or isFilePro(arg) or isFileApp(arg)

def isFile(arg):
    return isFileOut(arg) or isFileIn(arg)

def isPipe(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and ('l' in arg[0])

def isCon(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (arg[0] == 'c')

def isSeq(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (arg[0] == 's')

def isSucc(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (arg[0] == 'a')

def isFail(arg):
    return isinstance(arg,list) and isinstance(arg[0],str) and (arg[0] == 'v')

def isCtrl(arg):
    return isPipe(arg) or isCon(arg) or isSeq(arg) or isSucc(arg) or isFail(arg)

def save():
    if save_lock:
        print("Error! A source was set in \""+CONF_FILE+"\" but could not be read, used local parameters instead but these may not be changed.")
    else:
        try:
            cf = open(conf_file,'w')
            json.dump({"binds":binds,"alias":alias},cf,indent=TAB_VAL)
            cf.close()
        except:
            print("Error! Configuration file \""+conf_file+"\" could not be opened in write mode!")

def buildFunc(func):
    lst = []
    for e in func:
        if e[0] == ECHAR:
            if len(e) < 2:
                lst.append(ECHAR)
            elif e[1] == ECHAR:
                lst.append(e[1:])
            elif e[1:].isnumeric():
                lst.append(numArg(e[1:]))
            elif e[1] == '-':
                lst.append(['-'])
            elif e[1] == 'p':
                lst.append(['p',e[2:]])
            elif e[1] == 'n':
                lst.append(['n',e[2:]])
            elif e[1] == '=':
                lst.append(['=',e[2:]])
            elif e[1] == '/':
                if e[2] == 'p':
                    lst.append(["/p",e[3:]])
                elif e[2] == 'n':
                    lst.append(["/n",e[3:]])
                elif e[2] == '=':
                    lst.append(["/=",e[3:]])
            elif e[1] == 'i':
                lst.append(['i',e[2:]])
            elif e[1] == 'o':
                lst.append(['o',e[2:]])
            elif e[1] == 'x':
                lst.append(['x',e[2:]])
            elif e[1] == '+':
                lst.append(['+',e[2:]])
            elif e[1] == 'e':
                if e[2] == 'o':
                    lst.append(["eo",e[3:]])
                elif e[2] == 'x':
                    lst.append(["ex",e[3:]])
                elif e[2] == '+':
                    lst.append(["e+",e[3:]])
                elif e[2] == 'l':
                    lst.append(["el"])
            elif e[1] == 'c':
                lst.append(['c'])
            elif e[1] == 's':
                lst.append(['s'])
            elif e[1] == 'a':
                lst.append(['a'])
            elif e[1] == 'v':
                lst.append(['v'])
            elif e[1] == 'l':
                lst.append(['l'])
        else:
            lst.append(e)
    return lst

def addFunc(name,func,pos):
    if pos < 0:
        pos = 0
    if pos > len(binds):
        pos = len(binds)
    if name[0] == '_':
        print("Error! Names starting with \"_\" are reserved for builtin functions!")
    elif name in alias:
        print("Error! \""+name+"\" is bound!")
        print("You may unbind it with: sc _del "+name)
    elif name.isnumeric():
        print("Error! \""+name+"\" is a number (forbiden)!")
    else:
        binds.insert(pos,buildFunc(func))
        if pos < len(binds):
            for e in alias.items():
                if e[1] >= pos:
                    alias[e[0]]+=1
        if name != "":
            alias[name] = pos
        save()
        
def move(old,new):
    cmd = binds[old]
    del binds[old]
    if new < 0:
        new = 0
    if new > len(binds):
        new = len(binds)
    binds.insert(new,cmd)
    for e in alias.items():
        if e[1] == old:
            alias[e[0]] = new
        elif (e[1] < old) and (e[1] >= new):
            alias[e[0]]+=1
        elif (e[1] > old) and (e[1] <= new):
            alias[e[0]]-=1
    save()
        
def removeBind(pos):
    del binds[pos]
    global alias
    nalias = {}
    for e in alias.items():
        if e[1] < pos:
            nalias[e[0]] = alias[e[0]]
        elif e[1] > pos:
            nalias[e[0]] = (alias[e[0]]-1)
    alias = nalias
    save()
        
def remove(pos):
    del binds[pos]
    global alias
    nalias = {}
    for e in alias.items():
        if e[1] < pos:
            nalias[e[0]] = alias[e[0]]
        elif e[1] > pos:
            nalias[e[0]] = (alias[e[0]]-1)
    alias = nalias
    save()

def addSh(num,arg,name,val):
    for x in range(len(binds[num])):
        if eqArg(binds[num][x],arg):
            if isArg(binds[num][x]):
                binds[num][x] = [arg,{}]
            binds[num][x][1][name] = val
    save()

def delSh(num,arg,name):
    for x in range(len(binds[num])):
        if eqArg(binds[num][x],arg):
            if isDict(binds[num][x]):
                if name in binds[num][x][1]:
                    del binds[num][x][1][name]
                if (len(binds[num][x][1]) == 0) and (not hasDefault(binds[num][x])):
                    if isNumDict(binds[num][x]):
                        binds[num][x] = arg
                    elif isEndDict(binds[num][x]):
                        binds[num][x] = ['-']
    save()

def clearSh(num,arg):
    for x in range(len(binds[num])):
        if eqArg(binds[num][x],arg):
            if isNumDict(binds[num][x]):
                if hasDefault(binds[num][x]):
                    binds[num][x] = [arg,{},binds[num][x][2]]
                else:
                    binds[num][x] = arg
            elif isEndDict(binds[num][x]):
                if hasDefault(binds[num][x]):
                    binds[num][x] = ['-',{},binds[num][x][2]]
                else:
                    binds[num][x] = ['-']
    save()

def clearAllSh(num):
    for x in range(len(binds[num])):
        if isNumDict(binds[num][x]):
            if hasDefault(binds[num][x]):
                binds[num][x][1] = {}
            else:
                binds[num][x] = binds[num][x][0]
        elif isEndDict(binds[num][x]):
            if hasDefault(binds[num][x]):
                binds[num][x] = ['-',{},binds[num][x][2]]
            else:
                binds[num][x] = ['-']
    save()

def addDef(num,arg,val):
    for x in range(len(binds[num])):
        if eqArg(binds[num][x],arg):
            if isArg(binds[num][x]):
                binds[num][x] = [arg,{},'']
            elif not hasDefault(binds[num][x]):
                binds[num][x].append('')
            binds[num][x][2] = val
    save()

def delDef(num,arg):
    for x in range(len(binds[num])):
        if eqArg(binds[num][x],arg) and hasDefault(binds[num][x]):
            binds[num][x] = binds[num][x][0:1]
            if len(binds[num][x][1]) == 0:
                if isNumDict(binds[num][x]):
                    binds[num][x] = arg
                elif isEndDict(binds[num][x]):
                    binds[num][x] = ['-']
    save()

def clearDef(num):
    for x in range(len(binds[num])):
        binds[num][x] = binds[num][x][0:1]
        if len(binds[num][x][1]) == 0:
            if isNumDict(binds[num][x]):
                binds[num][x] = binds[num][x][0]
            elif isEndDict(binds[num][x]):
                binds[num][x] = ['-']
    save()
    
def cmdToStr(cmd):
    s = ""
    for e in cmd:
        if isNumArg(e):
            s+= (ECHAR+str(e+1))
        elif isEndArg(e):
            s+= (ECHAR+'-')
        elif isNumDict(e):
            s+= (ECHAR+str(e[0]+1))
            if len(e[1]) > 0:
                s+= str(e[1])
        elif isEndDict(e):
            s+= (ECHAR+'-')
            if len(e[1]) > 0:
                s+= str(e[1])
        elif isComp(e):
            if isCompPre(e):
                s = s[:-1]
                if isCompOpt(e):
                    s+= '/'
            s+= e[1]
        elif isFile(e):
            s+= (ECHAR+e[0]+e[1])
        elif isCtrl(e):
            s+= (ECHAR+e[0])
        else:
            s+= e
        if hasDefault(e):
            s+= ('['+str(e[2])+']')
        if not isCompNxt(e):
            s+= ' '
        elif isCompOpt(e):
            s+= '/'
    return s[:-1]

def listFuncs(fltr):
    i = 0
    for f in binds:
        cmd = cmdToStr(f)
        if fltr in cmd:
            sup = ""
            for e in alias.items():
                if e[1] == i:
                    sup+= ("| sc "+e[0]+' ')
            print('['+str(i+1)+"] sc "+str(i+1)+' '+sup+" :  "+cmd)
        i+=1

def command(cmd,args):
    exe = [[[],{}]]
    used = set()
    targ = 0
    for e in cmd:
        if isNumArg(e):
            if e >= targ:
                targ = (e+1)
        elif isNumDict(e):
            if e[0] >= targ:
                targ = (e[0]+1)
    comp = False
    opt = False
    empty = False
    for e in cmd:
        empty = False
        if isNumArg(e):
            if len(args) > e:
                exe[-1][0].append(args[e])
                used.add(e)
            elif hasDefault(e):
                exe[-1][0].append(e[2])
            else:
                empty = True
        elif isNumDict(e):
            if len(args) > e[0]:
                arg = args[e[0]]
                if arg in e[1]:
                    exe[-1][0].append(e[1][arg])
                else:
                    exe[-1][0].append(arg)
                used.add(e[0])
            elif hasDefault(e):
                exe[-1][0].append(e[2])
            else:
                empty = True
        elif isEndArg(e):
            for a in range(targ,len(args)):
                exe[-1][0].append(args[a])
                used.add(a)
            if len(args) <= targ:
                if hasDefault(e):
                    exe[-1][0].append(e[2])
                else:
                    empty = True
        elif isEndDict(e):
            for a in range(targ,len(args)):
                arg = args[a]
                if arg in e[1]:
                    exe[-1][0].append(e[1][arg])
                else:
                    exe[-1][0].append(arg)
                used.add(a)
            if len(args) <= targ:
                if hasDefault(e):
                    exe[-1][0].append(e[2])
                else:
                    empty = True
        elif isComp(e):
            exe[-1][0].append(e[1])
        elif isErrRed(e):
            exe[-1][1]["err"] = ()
            if isFileOut(e):
                exe[-1][1]["file"] = e[1]
                if isFileRep(e):
                    exe[-1][1]["fmod"] = 'w'
                elif isFilePro(e):
                    exe[-1][1]["fmod"] = 'x'
                elif isFileApp(e):
                    exe[-1][1]["fmod"] = 'a'
            elif isPipe(e):
                exe[-1][1]["pipe"] = ()
                exe.append([[],{"rcp":()}])
        elif isFileOut(e):
            exe[-1][1]["file"] = e[1]
            if isFileRep(e):
                exe[-1][1]["fmod"] = 'w'
            elif isFilePro(e):
                exe[-1][1]["fmod"] = 'x'
            elif isFileApp(e):
                exe[-1][1]["fmod"] = 'a'
        elif isPipe(e):
            exe[-1][1]["pipe"] = ()
            exe.append([[],{"rcp":()}])
        elif isFileIn(e):
            exe[-1][1]["in"] = e[1]
        elif isCon(e):
            exe[-1][1]["con"] = ()
            exe.append([[],{}])
        elif isSucc(e):
            exe.append([[],{}])
        elif isFail(e):
            exe[-1][1]["fail"] = ()
            exe.append([[],{}])
        elif isSeq(e):
            exe[-1][1]["seq"] = ()
            exe.append([[],{}])
        else:
            exe[-1][0].append(e)
        if isCompPre(e):
            if isCompOpt(e) and empty:
                exe[-1][0] = exe[-1][0][:-1]
            elif len(exe[-1][0]) > 1:
                exe[-1][0] = exe[-1][0][:-2]+[(exe[-1][0][-2]+exe[-1][0][-1])]
        elif comp:
            if opt and empty:
                exe[-1][0] = exe[-1][0][:-1]
            if len(exe[-1][0]) > 1:
                exe[-1][0] = exe[-1][0][:-2]+[(exe[-1][0][-2]+exe[-1][0][-1])]
            comp = False
        opt = False
        if isCompNxt(e):
            comp = True
            if isCompOpt(e):
                opt = True
    for i in range(len(args)):
        if not (i in used):
            exe[-1][0].append(args[i])
    return exe

def launch(cmd,args):
    exe = command(cmd,args)
    for c in exe:
        stin = None
        stout = None
        sterr = None
        if "rcp" in c[1]:
            stin = mem
        if "err" in c[1]:
            sterr = subprocess.STDOUT
        if "pipe" in c[1]:
            stout = subprocess.PIPE
        if "file" in c[1]:
            stout = open(c[1]["file"],c[1]["fmod"])
        if "in" in c[1]:
            stin = open(c[1]["in"],'r')
        if "con" in c[1]:
            if isinstance(stin,bytes):
                subprocess.Popen(c[0],stdin=subprocess.PIPE,stdout=stout,stderr=sterr).communicate(input=stin)
            else:
                subprocess.Popen(c[0],stdin=stin,stdout=stout,stderr=sterr)
        else:
            try:
                if isinstance(stin,bytes):
                    mem = subprocess.run(c[0],input=stin,stdout=stout,stderr=sterr,check=True).stdout
                else:
                    mem = subprocess.run(c[0],stdin=stin,stdout=stout,stderr=sterr,check=True).stdout
                if "fail" in c[1]:
                    break
            except:
                if not (("fail" in c[1]) or ("seq" in c[1])):
                    raise

def test(cmd,args):
    exe = command(cmd,args)
    i = 1
    for c in exe:
        cmd = "["
        stin = "cli"
        stout = "cli"
        sterr = "cli"
        if "rcp" in c[1]:
            stin = "pipe"
        if "err" in c[1]:
            sterr = "stdout"
        if "pipe" in c[1]:
            stout = "pipe"
        if "file" in c[1]:
            stout = "file<"+c[1]["fmod"]+">:"+c[1]["file"]
        if "in" in c[1]:
            stin = "file:"+c[1]["in"]
        if "con" in c[1]:
            cmd+= "DETACH,"
        cmd+= "stdin="+stin+','
        cmd+= "stdout="+stout+','
        cmd+= "stderr="+sterr+']'
        for a in c[0]:
            cmd+= ' '+a
        print(cmd)
        if "fail" in c[1]:
            print("[IF FAILURE]")
        elif (not ("seq" in c[1])) and i < len(exe):
            print("[IF SUCCESS]")
        i+=1
            
def source(f):
    cf = open(CONF_FILE,'w')
    if not os.path.isfile(f):
        try:
            ff = open(f,'w')
            json.dump({},ff,indent=TAB_VAL)
            ff.close
        except:
            print("Error! File :\""+f+"\" could not be opened nor created!")
            raise
    json.dump({"source":f,"binds":binds,"alias":alias},cf,indent=TAB_VAL)
    cf.close()

def unsource():
    cfile = open(CONF_FILE,'r')
    conf = json.load(cfile)
    if "binds" in conf:
        binds = list(conf["binds"])
    if "alias" in conf:
        alias = dict(conf["alias"])
    cf = open(CONF_FILE,'w')
    json.dump({"binds":binds,"alias":alias},cf,indent=TAB_VAL)
    cf.close()

def argCount(n):
    if len(sys.argv) <= n:
        print("Error: Incorrect argument count!")
        sys.exit(1)


binds = []
alias = {}
save_lock = False
conf_file = CONF_FILE
if os.path.isfile(CONF_FILE):
    cfile = open(CONF_FILE,'r')
    try:
        conf = json.load(cfile)
        if "source" in conf:
            try:
                conf_file = conf["source"]
                scfile = open(os.path.normpath(os.path.expanduser(conf_file)),'r')
                conf = json.load(scfile)
                scfile.close()
            except:
                conf_file = CONF_FILE
                print("Warning! A source was set in \""+CONF_FILE+"\" but could not be read, using local parameters instead but these may not be changed.")
                cfile.seek(0)
                conf = json.load(cfile)
                save_lock = True
        if "binds" in conf:
            binds = list(conf["binds"])
        if "alias" in conf:
            alias = dict(conf["alias"])
    except:
        ()
    cfile.close()


argCount(1)
func = sys.argv[1]

if (func == "_n") or (func == "_new"):
    argCount(3)
    if sys.argv[2].isnumeric():
        argCount(4)
        addFunc(sys.argv[3],sys.argv[4:],numArg(sys.argv[2]))
    else:
        addFunc(sys.argv[2],sys.argv[3:],len(binds))
elif (func == "_i") or (func == "_insert"):
    argCount(3)
    addFunc("",sys.argv[3:],numArg(sys.argv[2]))
elif (func == "_p") or (func == "_push"):
    argCount(2)
    addFunc("",sys.argv[2:],len(binds))
elif (func == "_a") or (func == "_add"):
    argCount(3)
    if sys.argv[3].isnumeric():
        if int(sys.argv[3]) <= len(binds):
            alias[sys.argv[2]] = numArg(sys.argv[3])
            save()
        else:
            print("Error! \""+sys.argv[3]+"\" greater than the total number of registered functions!")
    elif sys.argv[3] in alias:
        alias[sys.argv[2]] = alias[sys.argv[3]]
        save()
    else:
        print("Error! \""+sys.argv[2]+"\" is not a registered alias!")
elif (func == "_d") or (func == "_del"):
    argCount(2)
    del alias[sys.argv[2]]
    save()
elif (func == "_r") or (func == "_rm"):
    argCount(2)
    if sys.argv[2].isnumeric():
        remove(numArg(sys.argv[2]))
    else:
        remove(alias[sys.argv[2]])
elif (func == "_rk") or (func == "_rank"):
    argCount(3)
    if sys.argv[2].isnumeric():
        move(numArg(sys.argv[2]),numArg(sys.argv[3]))
    else:
        move(alias[sys.argv[2]],numArg(sys.argv[3]))
elif (func == "_nm") or (func == "_name"):
    argCount(3)
    cmd = alias[sys.argv[2]]
    del alias[sys.argv[2]]
    alias[sys.argv[3]] = cmd
    save()
elif (func == "_l") or (func == "_list"):
    listFuncs("")
elif (func == "_pr") or (func == "_print"):
    argCount(2)
    if sys.argv[2].isnumeric():
        print(cmdToStr(binds[numArg(sys.argv[2])]))
    else:
        print(cmdToStr(binds[alias[sys.argv[2]]]))
elif (func == "_f") or (func == "_find"):
    argCount(2)
    listFuncs(sys.argv[2])
elif (func == "_s") or (func == "_short"):
    argCount(5)
    if sys.argv[2].isnumeric():
        addSh(numArg(sys.argv[2]),numArg(sys.argv[3]),sys.argv[4],sys.argv[5])
    else:
        addSh(alias[sys.argv[2]],numArg(sys.argv[3]),sys.argv[4],sys.argv[5])
elif (func == "_sd") or (func == "_short-del"):
    argCount(4)
    if sys.argv[2].isnumeric():
        delSh(numArg(sys.argv[2]),numArg(sys.argv[3]),sys.argv[4])
    else:
        delSh(alias[sys.argv[2]],numArg(sys.argv[3]),sys.argv[4])
elif (func == "_sk") or (func == "_short-clear"):
    argCount(2)
    if sys.argv[2].isnumeric():
        num = numArg(sys.argv[2])
    else:
        num = alias[sys.argv[2]]
    if len(sys.argv) > 3:
        clearSh(num,numArg(sys.argv[3]))
    else:
        clearAllSh(num)
elif (func == "_e") or (func == "_def"):
    argCount(3)
    if len(sys.argv) > 4:
        if sys.argv[2].isnumeric():
            addDef(numArg(sys.argv[2]),numArg(sys.argv[3]),sys.argv[4])
        else:
            addDef(alias[sys.argv[2]],numArg(sys.argv[3]),sys.argv[4])
    else:
        if sys.argv[2].isnumeric():
            delDef(numArg(sys.argv[2]),numArg(sys.argv[3]))
        else:
            delDef(alias[sys.argv[2]],numArg(sys.argv[3]))
elif (func == "_dc") or (func == "_def-clear"):
    argCount(2)
    if sys.argv[2].isnumeric():
        num = numArg(sys.argv[2])
    else:
        num = alias[sys.argv[2]]
    if len(sys.argv) > 3:
        delDef(num,numArg(sys.argv[3]))
    else:
        clearDef(num)
elif (func == "_c") or (func == "_change"):
    argCount(2)
    if sys.argv[2] in alias:
        binds[alias[sys.argv[2]]] = buildFunc(sys.argv[3:])
    elif sys.argv[2].isnumeric() and (int(sys.argv[2]) < len(binds)):
        binds[numArg(sys.argv[2])] = buildFunc(sys.argv[3:])
    else:
        print("Error! Function \""+sys.argv[2]+"\" is not valid!")
    save()
elif (func == "_t") or (func == "_test"):
    argCount(2)
    if sys.argv[2] in alias:
        test(binds[alias[sys.argv[2]]],sys.argv[3:])
    elif sys.argv[2].isnumeric() and (int(sys.argv[2]) < len(binds)):
        test(binds[numArg(sys.argv[2])],sys.argv[3:])
    else:
        print("Error! Function \""+sys.argv[2]+"\" is not valid!")
elif (func == "_sr") or (func == "_source"):
    if len(sys.argv) > 2:
        source(sys.argv[2])
    else:
        unsource()
elif func in alias:
    launch(binds[alias[func]],sys.argv[2:])
elif func.isnumeric() and (int(func) < len(binds)):
    launch(binds[numArg(func)],sys.argv[2:])
else:
    print("Error! Function \""+func+"\" is not valid!")
